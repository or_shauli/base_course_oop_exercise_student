package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehiclesMissionAbility.IntelligenceJet;
import Entities.Coordinates;

public class IntelligenceMission extends Mission{
    private String region;
    public IntelligenceMission(Coordinates missionLocation, String pilotName, AerialVehicle aerialVehicle,
                               String region) {
        super(missionLocation, pilotName, aerialVehicle);
        this.region = region;
    }

    @Override
    String executeMission() throws AerialVehicleNotCompatibleException {
        if (this.getAerialVehicle().hasAbility(IntelligenceJet.class)){
            return this.getPilotName() + ": " + this.getAerialVehicle().getClass().getSimpleName() +
                     " collecting data in " +
                    this.region + " with " +
                    this.getAerialVehicle().getAbility(IntelligenceJet.class).toString() + " ";
        }
        throw new AerialVehicleNotCompatibleException("this AerialVehicle " +
                this.getAerialVehicle().getClass().getSimpleName() +
                " not build for intelligenceMission missons !!!");
    }
}
