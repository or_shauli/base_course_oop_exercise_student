package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehiclesMissionAbility.BdaJet;
import Entities.Coordinates;

public class BdaMission extends Mission{
    private String objective;

    public BdaMission(Coordinates missionLocation, String pilotName, AerialVehicle aerialVehicle, String objective) {
        super(missionLocation, pilotName, aerialVehicle);
        this.objective = objective;
    }

    @Override
    String executeMission() throws AerialVehicleNotCompatibleException {
        if (this.getAerialVehicle().hasAbility(BdaJet.class)){
            return this.getPilotName() + ": " + this.getAerialVehicle().getClass().getSimpleName() +
                    " taking pictures of " +
                    this.objective + " with " + this.getAerialVehicle().getAbility(BdaJet.class).toString() + " ";
        }
        throw new AerialVehicleNotCompatibleException("this AerialVehicle " +
                this.getAerialVehicle().getClass().getSimpleName() +
                " not build for bda missons !!!");
    }
}
