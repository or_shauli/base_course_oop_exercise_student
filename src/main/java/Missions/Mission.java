package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission{
    private Coordinates missionLocation;
    private String pilotName;
    private AerialVehicle aerialVehicle;

    public Mission(Coordinates missionLocation, String pilotName, AerialVehicle aerialVehicle) {
        this.missionLocation = missionLocation;
        this.pilotName = pilotName;
        this.aerialVehicle = aerialVehicle;
    }

    public void begin(){
        System.out.println("Beginning Mission!");
        this.aerialVehicle.flyTo(this.missionLocation);
    }

    public void cancel(){
        System.out.println("Abort Mission!");
        Coordinates motherBase = this.aerialVehicle.getMotherBase();
        this.aerialVehicle.land(motherBase);
    }

    public void finish(){
        try {
            String missionVerbose  = this.executeMission();
            System.out.println(missionVerbose);
            Coordinates motherBase = this.aerialVehicle.getMotherBase();
            this.aerialVehicle.land(motherBase);
            System.out.println("Finish Mission!");
        } catch (AerialVehicleNotCompatibleException exception){
            System.out.println(exception.toString());
        }
    }

    abstract String executeMission() throws AerialVehicleNotCompatibleException;

    public Coordinates getMissionLocation() {
        return missionLocation;
    }

    public void setMissionLocation(Coordinates missionLocation) {
        this.missionLocation = missionLocation;
    }

    public String getPilotName() {
        return pilotName;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public AerialVehicle getAerialVehicle() {
        return aerialVehicle;
    }

    public void setAerialVehicle(AerialVehicle aerialVehicle) {
        this.aerialVehicle = aerialVehicle;
    }
}
