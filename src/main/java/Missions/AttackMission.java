package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehiclesMissionAbility.AttackJet;
import Entities.Coordinates;

public class AttackMission extends Mission{
    private String target;

    public AttackMission(Coordinates missionLocation, String pilotName, AerialVehicle aerialVehicle, String target) {
        super(missionLocation, pilotName, aerialVehicle);
        this.target = target;
    }

    @Override
    String executeMission() throws AerialVehicleNotCompatibleException {
        if (this.getAerialVehicle().hasAbility(AttackJet.class)){
            return this.getPilotName() + ": " + this.getAerialVehicle().getClass().getSimpleName() +
                    " attacking " +
                    this.target + " with " + this.getAerialVehicle().getAbility(AttackJet.class).toString() + " ";
        }
        throw new AerialVehicleNotCompatibleException("this AerialVehicle " +
                this.getAerialVehicle().getClass().getSimpleName() +
                " not build for attack missons !!!");
    }
}
