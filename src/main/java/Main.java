import AerialVehicles.*;
import AerialVehicles.Fighters.F15;
import AerialVehicles.UnmannedAircraft.Haron.Eitan;
import AerialVehicles.UnmannedAircraft.Hermes.Kochav;
import AerialVehicles.UnmannedAircraft.UnmannedAircraft;
import AerialVehiclesMissionAbility.AttackJet;
import AerialVehiclesMissionAbility.BdaJet;
import AerialVehiclesMissionAbility.IntelligenceJet;
import AerialVehiclesMissionAbility.JetAbility;
import Entities.Coordinates;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;
import Missions.Mission;
import enums.BombType;
import enums.CameraType;
import enums.SensorType;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<JetAbility> f15Abilities = new ArrayList<>();
        f15Abilities.add(new AttackJet(3, BombType.AMRAM));
        f15Abilities.add(new IntelligenceJet(SensorType.INFRA_RED));

        ArrayList<JetAbility> eitanAbilities = new ArrayList<>();
        eitanAbilities.add(new IntelligenceJet(SensorType.ELINT));
        eitanAbilities.add(new AttackJet(25, BombType.SPICE250));

        ArrayList<JetAbility> kochavAbilities = new ArrayList<>();
        kochavAbilities.add(new IntelligenceJet(SensorType.ELINT));
        kochavAbilities.add(new AttackJet(60, BombType.PYTHON));
        kochavAbilities.add(new BdaJet(CameraType.NIGHT_VISION));



        AerialVehicle f15 = new F15(new Coordinates(10.1, 10.1),f15Abilities);

        UnmannedAircraft eitan = new Eitan(new Coordinates(45.1, 45.4), eitanAbilities);

        AerialVehicle kochav = new Kochav(new Coordinates(67.3, 67.1), kochavAbilities);

       f15.flyTo(new Coordinates(4.3,4.3));
       System.out.println(eitan.hoverOverLocation(new Coordinates(777.7, 777.7)));

        Mission attackMission = new AttackMission(new Coordinates(4.0,4.0)
                                            ,"tahel",kochav,"rishon");
        attackMission.begin();
        attackMission.finish();


        Mission bdaMission = new BdaMission(new Coordinates(4.0,4.0)
                ,"ani",f15,"gaza");
        bdaMission.begin();
        bdaMission.finish();

        f15.land(f15.getMotherBase());
        f15.setHoursSinceLastRepair(234);
        f15.check();

        Mission intelligenceMission = new IntelligenceMission(new Coordinates(4.0,4.0)
                ,"or",f15,"haifa");
        intelligenceMission.begin();
        intelligenceMission.finish();

        Mission secondBdaMission = new BdaMission(new Coordinates(4.0,4.0)
                ,"tami",kochav,"luv");
        secondBdaMission.begin();
        secondBdaMission.finish();
    }
}
