package AerialVehiclesMissionAbility;

import enums.SensorType;

public class IntelligenceJet extends JetAbility {
    private SensorType sensorType;

    public IntelligenceJet(SensorType sensorType) {
        this.sensorType = sensorType;
    }

    public SensorType getSensorType() {
        return sensorType;
    }

    public String toString() {
        return " sensor type " + this.sensorType.toString();
    }

    public void setSensorType(SensorType sensorType) {
        this.sensorType = sensorType;
    }
}
