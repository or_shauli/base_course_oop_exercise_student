package AerialVehiclesMissionAbility;

import enums.CameraType;

public class BdaJet extends JetAbility {
    private CameraType cameraType;

    public BdaJet(CameraType cameraType) {
        this.cameraType = cameraType;
    }

    public CameraType getCameraType() {
        return cameraType;
    }

    public void setCameraType(CameraType cameraType) {
        this.cameraType = cameraType;
    }

    public String toString() {
        return this.cameraType.toString() + " camera ";
    }
}
