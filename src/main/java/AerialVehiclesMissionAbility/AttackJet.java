package AerialVehiclesMissionAbility;

import enums.BombType;

public class AttackJet extends JetAbility {
    private int bombsAmount;
    private BombType bombType;

    public AttackJet(int bombsAmount, BombType bombType) {
        this.bombsAmount = bombsAmount;
        this.bombType = bombType;
    }

    public String toString() {
        return this.bombType.toString() + " X " + this.bombsAmount;
    }

    public int getBombsAmount() {
        return bombsAmount;
    }

    public void setBombsAmount(int bombsAmount) {
        this.bombsAmount = bombsAmount;
    }

    public BombType getBombType() {
        return bombType;
    }

    public void setBombType(BombType bombType) {
        this.bombType = bombType;
    }
}
