package AerialVehicles;

import AerialVehiclesMissionAbility.JetAbility;
import Entities.Coordinates;
import Missions.AerialVehicleNotCompatibleException;
import enums.FlightStatus;

import java.util.ArrayList;

public abstract class AerialVehicle {
    private int hoursSinceLastRepair;
    private FlightStatus flightStatus;
    private Coordinates motherBase;
    private ArrayList<JetAbility> abilities;

    public AerialVehicle(Coordinates motherBase, ArrayList<JetAbility> abilities){
        this.hoursSinceLastRepair = 0;
        this.flightStatus = FlightStatus.READY;
        this.motherBase = motherBase;
        this.abilities = abilities;
    }

    public void addAbility(JetAbility jetAbility){
        this.abilities.add(jetAbility);
    }

    public void removeAbility(JetAbility jetAbility){
        String abilityToDelete =    jetAbility.getClass().getSimpleName();
        this.abilities.removeIf((ability) -> ability.getClass().getSimpleName().equals(abilityToDelete));
    }

    public boolean hasAbility(Class <?> jetAbilityClass){
        for (JetAbility currentAbility : this.abilities){
            if (currentAbility.getClass().getSimpleName().equals(jetAbilityClass.getSimpleName())){
                return true;
            }
        }
        return false;
    }

    public JetAbility getAbility(Class <?> jetAbilityClass) throws AerialVehicleNotCompatibleException {
        for (JetAbility currentAbility : this.abilities){
            if (currentAbility.getClass().getSimpleName().equals(jetAbilityClass.getSimpleName())){
                return currentAbility;
            }
        }
        throw new AerialVehicleNotCompatibleException("this jet is not " + jetAbilityClass.getClass().getSimpleName());
    }

    public void flyTo(Coordinates destination){
        if (this.flightStatus == FlightStatus.READY){
            System.out.println("flying to " + destination.toString());
            this.flightStatus = FlightStatus.FLYING;
        }
        if (this.flightStatus == FlightStatus.NOT_READY){
            System.out.println("Aerial Vehicle isn't ready to fly");
        }
    }

    public void land (Coordinates destination){
        System.out.println("landing on " + destination.toString());
        this.check();
    }

    public void check (){
        if(this.getHoursSinceLastRepair() >= this.getMaxHoursOfFlyingWithoutRepair()){
            this.setFlightStatus(FlightStatus.NOT_READY);
            this.repair();
        }else{
            this.setFlightStatus(FlightStatus.READY);
        }
    }

    public abstract int getMaxHoursOfFlyingWithoutRepair();

    public void repair(){
        this.hoursSinceLastRepair = 0;
        this.flightStatus = FlightStatus.READY;
    }

    public int getHoursSinceLastRepair() {
        return hoursSinceLastRepair;
    }

    public void setHoursSinceLastRepair(int hoursSinceLastRepair) {
        this.hoursSinceLastRepair = hoursSinceLastRepair;
    }

    public FlightStatus getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    public Coordinates getMotherBase() {
        return motherBase;
    }

    public void setMotherBase(Coordinates motherBase) {
        this.motherBase = motherBase;
    }

    public ArrayList<JetAbility> getAbilities() {
        return this.abilities;
    }

    public void setAbilities(ArrayList<JetAbility> abilities) {
        this.abilities = abilities;
    }
}
