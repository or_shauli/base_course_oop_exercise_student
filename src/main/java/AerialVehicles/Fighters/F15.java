package AerialVehicles.Fighters;


import AerialVehicles.Fighters.FighterJet;
import AerialVehiclesMissionAbility.JetAbility;
import Entities.Coordinates;

import java.util.ArrayList;

public class F15 extends FighterJet {
    private final int NEED_REPAIR_AFTER_THIS_FLYING_HOURS = 250;

    public F15(Coordinates motherBase, ArrayList<JetAbility> abilities) {
        super(motherBase, abilities);
    }

    @Override
    public int getMaxHoursOfFlyingWithoutRepair() {
        return this.NEED_REPAIR_AFTER_THIS_FLYING_HOURS;
    }


}
