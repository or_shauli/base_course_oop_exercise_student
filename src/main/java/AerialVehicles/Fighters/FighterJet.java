package AerialVehicles.Fighters;

import AerialVehicles.AerialVehicle;
import AerialVehiclesMissionAbility.JetAbility;
import Entities.Coordinates;
import enums.FlightStatus;

import java.util.ArrayList;

public abstract class FighterJet extends AerialVehicle {

    public FighterJet(Coordinates motherBase, ArrayList<JetAbility> abilities) {
        super(motherBase, abilities);
    }
}
