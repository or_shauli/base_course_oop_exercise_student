package AerialVehicles.UnmannedAircraft.Hermes;

import AerialVehicles.UnmannedAircraft.Hermes.HermesJet;
import AerialVehiclesMissionAbility.JetAbility;
import Entities.Coordinates;

import java.util.ArrayList;

public class Kochav extends HermesJet {

    public Kochav(Coordinates motherBase, ArrayList<JetAbility> abilities) {
        super(motherBase, abilities);
    }

}
