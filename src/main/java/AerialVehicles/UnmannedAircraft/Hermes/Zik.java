package AerialVehicles.UnmannedAircraft.Hermes;


import AerialVehicles.UnmannedAircraft.Hermes.HermesJet;
import AerialVehiclesMissionAbility.JetAbility;
import Entities.Coordinates;

import java.util.ArrayList;

public class Zik extends HermesJet {

    public Zik(Coordinates motherBase, ArrayList<JetAbility> abilities) {
        super(motherBase, abilities);
    }
}