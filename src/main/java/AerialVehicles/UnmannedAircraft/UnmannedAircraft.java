package AerialVehicles.UnmannedAircraft;

import AerialVehicles.AerialVehicle;
import AerialVehiclesMissionAbility.JetAbility;
import Entities.Coordinates;
import enums.FlightStatus;

import java.util.ArrayList;

public abstract class UnmannedAircraft extends AerialVehicle {


    public UnmannedAircraft(Coordinates motherBase, ArrayList<JetAbility> abilities) {
        super(motherBase, abilities);
    }

    public String hoverOverLocation(Coordinates destination){
        this.setFlightStatus(FlightStatus.FLYING);
        return "Hovering over " + destination.toString();
    }


}
