package AerialVehicles.UnmannedAircraft.Haron;

import AerialVehicles.UnmannedAircraft.Haron.HaronJet;
import AerialVehiclesMissionAbility.JetAbility;
import Entities.Coordinates;

import java.util.ArrayList;

public class Eitan extends HaronJet {

    public Eitan(Coordinates motherBase, ArrayList<JetAbility> abilities) {
        super(motherBase, abilities);
    }

}
