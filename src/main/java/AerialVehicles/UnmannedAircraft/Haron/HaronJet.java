package AerialVehicles.UnmannedAircraft.Haron;

import AerialVehicles.UnmannedAircraft.UnmannedAircraft;
import AerialVehiclesMissionAbility.JetAbility;
import Entities.Coordinates;

import java.util.ArrayList;

public abstract class HaronJet extends UnmannedAircraft {
    private final int NEED_REPAIR_AFTER_THIS_FLYING_HOURS = 150;

    public HaronJet(Coordinates motherBase, ArrayList<JetAbility> abilities) {
        super(motherBase, abilities);
    }


    @Override
    public int getMaxHoursOfFlyingWithoutRepair() {
        return this.NEED_REPAIR_AFTER_THIS_FLYING_HOURS;
    }


}
